create table Interpret(
	IntID integer primary key autoincrement,
	IntName varchar(255) not null unique default 'unknown'
	);

create table Medium(
	MedID integer primary key autoincrement,
	MedTyp varchar(255) unique
	);

create table Genre(
	GenID integer primary key autoincrement,
	GenName varchar(255) unique
	);


create table Titel(
	TitID integer primary key autoincrement,
	TitName varchar(255) not null,
	TitLen decimal
	);

create table Album(
	AlbID integer primary key autoincrement,
	EanNr varchar(255),
	MedID integer references Medium (MedID) on update cascade on delete set null,
	AlbName varchar(255) not null,
	AlbInterpret integer not null references Interpret (IntID) on update cascade on delete cascade,
	GenID integer references Genre (GenID) on update cascade on delete set null,
	AlbRating integer,
	AlbCover varchar(255) default '\pics\no-icon.png'	
	);

create table AlbumTitel(
	AlbID integer references Album (AlbID) on update cascade on delete cascade,
	TitID integer references Titel (TitID) on update cascade on delete cascade,
	primary key(AlbID,TitID)
	);
	
insert into Interpret (IntName) values ('Various Artists');   


