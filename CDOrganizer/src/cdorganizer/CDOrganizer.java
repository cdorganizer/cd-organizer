package cdorganizer;

import java.net.URL;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Screen;
import javafx.stage.Stage;

public class CDOrganizer extends Application {
    private FXMLLoader fxmlLoader;
    @Override
    public void start(Stage stage) throws Exception {
        URL location = getClass().getResource("cdorg.fxml");
        fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(location);
        fxmlLoader.setBuilderFactory(new JavaFXBuilderFactory());
        Parent root = (Parent) fxmlLoader.load(location.openStream());
        //Parent root = FXMLLoader.load(getClass().getResource("cdorg.fxml"));

        Scene scene = new Scene(root);

        Screen screen = Screen.getPrimary();        //Vorläufig wird alles bis set Scene
        Rectangle2D bounds = screen.getVisualBounds(); // benötigt für pseudo "maximized"

        stage.setX(bounds.getMinX());
        stage.setY(bounds.getMinY());
        stage.setWidth(bounds.getWidth());
        stage.setHeight(bounds.getHeight());
        stage.setScene(scene);
        stage.show();

        /*stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
         @Override
         public void handle(WindowEvent t) {
                
         }
         });*/

    }
    /*
     * Close Event wird an den cdorgController weitergeleitet und dort separat
     * verarbeitet
     */
    public void stop(){
        ((cdorgController) fxmlLoader.getController()).handleMenuClose(null);
    }
    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
}
