/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cdorganizer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import org.json.JSONException;

/**
 *
 * @author Philipp
 */
public class CDOrgDiscogsSearchTask extends TimerTask {

    private CDOrgAlbum inputAlbum;
    private CDOrgAlbum tempAlbum;
    private ArrayList<CDOrgAlbum> resultList;
    private final DiscogsInterface discogsInterface;
    private final int results;
    private final cdorgController controller;
    private Boolean isRunning;

    public CDOrgDiscogsSearchTask(cdorgController cdctrl, int results,CDOrgAlbum input) {
        super();
        inputAlbum = new CDOrgAlbum();
        discogsInterface = new DiscogsInterface();
        this.results = results;
        controller = cdctrl;
        isRunning = false;
        inputAlbum = input;
    }

    @Override
    public void run() {
        System.out.println("A SEARCH TASK STARTED");
        //cdctrl.getSearchProgress().setVisible(true);
        isRunning = true;
        /*try {
         //resultList = discogsInterface.discogsRequest(inputAlbum, results);
         } catch (JSONException | IOException ex) {
         Logger.getLogger(CDOrgDiscogsSearchTask.class.getName()).log(Level.SEVERE, null, ex);
         }*/
        //cdctrl.newSearchResults(resultList);//neue Ergebnissliste erstellen

        try {
            //outputAlbums = discogsInterface.discogsRequest(inputAlbum,25);
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    controller.getSearchProgress().setVisible(true);
                }
            });
            discogsInterface.singleRequest(inputAlbum, 50);
            //controller.addSearchResult(tempAlbum);

            for (int i = 0; i < 50; i++) {
                tempAlbum = discogsInterface.findNext();
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            controller.addSearchResult(CDOrgDiscogsSearchTask.this.tempAlbum);
                        } catch (IOException ex) {
                            Logger.getLogger(CDOrgDiscogsSearchTask.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                });

            }
        } catch (JSONException | IOException ex) {
            Logger.getLogger(CDOrgDiscogsSearchTask.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println("A SEARCH TASK STOPPED");
        //cdctrl.getSearchProgress().setVisible(false);
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                controller.getSearchProgress().setVisible(false);
            }
        });
        this.cancel();
    }

    public ArrayList<CDOrgAlbum> getResults() {
        return resultList;
    }

    public void setAlbum(CDOrgAlbum album) {
        inputAlbum = album;
    }

    public Boolean isRunning() {
        return isRunning;
    }
}
