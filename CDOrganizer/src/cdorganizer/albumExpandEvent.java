/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cdorganizer;

import java.util.ArrayList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

/**
 *
 * @author Philipp
 */
public class albumExpandEvent {

    private final Integer index;
    private final EventHandler expand;
    private final EventHandler click;

    public albumExpandEvent(final Integer index, final ArrayList<CDOrgAlbumComponent> allcomps, final AnchorPane parent, final cdorgController cdctrl) {
        super();
        this.index = index;
        expand = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                //JOptionPane.showMessageDialog(null, allcomps.get(index).getIndex());

                if (allcomps.get(index).getState()) {
                    allcomps.get(index).Collapse();
                    for (int i = index + 1; i < allcomps.size(); i++) {
                        allcomps.get(i).shiftUp(allcomps.get(i).getTableHeight().intValue());
                    }
                    parent.setPrefHeight(parent.getHeight() - allcomps.get(index).getTableHeight());

                } else {
                    parent.setPrefHeight(parent.getHeight() + allcomps.get(index).getTableHeight());
                    allcomps.get(index).Expand();
                    for (int i = index + 1; i < allcomps.size(); i++) {
                        allcomps.get(i).shiftDown(allcomps.get(i).getTableHeight().intValue());
                    }

                }

            }
        };
        click = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent t) {
                //JOptionPane.showMessageDialog(null, "Klick on Image");
                cdctrl.handleInsertButtonAction(null);

                cdctrl.refreshEditCheckBox(Boolean.FALSE);
                CDOrgAlbum temp = allcomps.get(index).getAlbum();
                cdctrl.getGenreField().setText(temp.getGenre());
                cdctrl.getAlbumField().setText(temp.getName());
                cdctrl.getEANField().setText(temp.getEAN());
                cdctrl.getInterpretField().setText(temp.getInterpret());
                cdctrl.getYearField().setText(temp.getYear().toString());
                cdctrl.getCompilationCheckBox().setSelected(temp.getIsCompilation());
                cdctrl.getCDCheckBox().setSelected(temp.getIsCD());
                cdctrl.getTapeCheckBox().setSelected(temp.getIsTape());
                cdctrl.getVinylCheckBox().setSelected(temp.getIsVinyl());
                cdctrl.getDigitalCheckBox().setSelected(temp.getIsDigital());
                cdctrl.getRatingChoiceBox().getSelectionModel().select(temp.getRating() - 1);
                cdctrl.setCurrentIndex(index);
            }
        };
    }

    public Integer getIndex() {
        return index;
    }

    public EventHandler getExpandEvent() {
        return expand;
    }

    public EventHandler getClickEvent() {
        return click;
    }
}
