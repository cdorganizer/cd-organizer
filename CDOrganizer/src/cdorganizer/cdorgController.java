package cdorganizer;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SplitPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.Rectangle2D;
import javafx.scene.control.TabPane;
import javafx.scene.control.ToolBar;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import javafx.stage.Screen;
import javax.swing.JOptionPane;
import org.json.JSONException;

public class cdorgController implements Initializable {

    @FXML
    private GridPane mainGridPane;
    @FXML
    private Button searchButton;
    @FXML
    private TextField eanField;
    @FXML
    private MenuItem Menuclose;
    @FXML
    private TextField albumField;
    @FXML
    private TextField interpretField;
    @FXML
    private TextField yearField;
    @FXML
    private TextField genreField;
    @FXML
    private TextField insertTitleField;
    @FXML
    private Button addTitleButton;
    private ArrayList<CDOrgNewTitleComponent> newTitles;
    private ArrayList<CDOrgTitle> cdorgTitles;
    private ArrayList<titleDeleteEvent> deleteEvents;
    @FXML
    private Button insertButton;
    @FXML
    private Button deleteButton;
    @FXML
    private Button acceptButton;    //Übernehmen Button
    @FXML
    private Button searchCoverButton;
    @FXML
    private CheckBox editCheckBox;
    @FXML
    private CheckBox compilationCheckBox;
    @FXML
    private CheckBox cdCheckBox;
    @FXML
    private CheckBox digitalCheckBox;
    @FXML
    private CheckBox vinylCheckBox;
    @FXML
    private CheckBox tapeCheckBox;
    @FXML
    private ChoiceBox ratingChoiceBox;
    @FXML
    private ImageView coverImage;
    @FXML
    private Tab localDatabaseTab;
    @FXML
    private Tab discogsDatabaseTab;
    @FXML
    private ScrollPane localDatabasePane;
    @FXML
    private ScrollPane discogsDataBasePane;
    @FXML
    private ChoiceBox searchTypeChoice; //nach einem oder allen Titeln suchen
    @FXML
    private MenuItem menuImport;
    @FXML
    private MenuItem menuExportHtml;
    @FXML
    private ProgressIndicator searchProgress;
    @FXML
    private SplitPane mainSplitPane;
    @FXML
    private AnchorPane titlesPane;
    @FXML
    private AnchorPane localDatabaseScrollAnchor;
    @FXML
    private AnchorPane discogsDataBaseScrollAnchor;
    @FXML
    private TabPane tabPane;
    @FXML
    private Button minimizeMultiMenu;
    @FXML
    private AnchorPane rightSide;
    @FXML
    private ToolBar menuToolbar;
    @FXML 
    private ScrollPane rightAlbumInsert;
    @FXML
    private Pane rightAlbumProperties;
    @FXML
    private GridPane rightAlbumInfo;
    @FXML 
    private Pane rightAlbumInfoBackground;
    @FXML
    private Text ratingText;
    private CDOrgAlbumComponent testcomp;
    private ArrayList<CDOrgAlbumComponent> albumcomponents;
    private ArrayList<albumExpandEvent> albumEvents;
    private ArrayList<CDOrgAlbum> searchedAlbums;
    private CDOrgAlbum tempAlbum;
    private File coverFile;
    private Integer currentSelectedIndex;
    private Boolean menuIsMinimized;
    private Double idealRatio;
    //private DiscogsInterface discogsInterface;
    private DiscogsSearchThread discogsSearchThread;
    private SearchCheckerThread searchCheck;
    
    @FXML
    private void handleSearchButtonAction(ActionEvent event) {
        //Suchen Button gedrückt
        searchTypeChoice.setVisible(true);
        acceptButton.setDisable(false);
        searchButton.defaultButtonProperty().set(true);
        insertButton.defaultButtonProperty().set(false);
        editCheckBox.setDisable(true);

        ratingChoiceBox.setDisable(true);
        ratingText.setDisable(true);
        coverImage.setDisable(true);
        searchCoverButton.setDisable(true);
        compilationCheckBox.setDisable(true);
        cdCheckBox.setDisable(true);
        vinylCheckBox.setDisable(true);
        tapeCheckBox.setDisable(true);
        digitalCheckBox.setDisable(true);

    }

    @FXML
    private void handleInsertButtonAction(ActionEvent event) {
        //Einfügen Button gedrückt
        searchTypeChoice.setVisible(false);
        acceptButton.setDisable(false);
        searchButton.defaultButtonProperty().set(false);
        insertButton.defaultButtonProperty().set(true);
        editCheckBox.setDisable(false);

        ratingChoiceBox.setDisable(false);
        ratingText.setDisable(false);
        coverImage.setDisable(false);
        searchCoverButton.setDisable(false);
        compilationCheckBox.setDisable(false);
        cdCheckBox.setDisable(false);
        vinylCheckBox.setDisable(false);
        tapeCheckBox.setDisable(false);
        digitalCheckBox.setDisable(false);
    }

    @FXML
    private void handleAcceptButtonAction(ActionEvent event) throws IOException, JSONException { //Übernehmen Button
        if (!"".equals(yearField.getText())) {
            tempAlbum.setYear(yearField.getText());
        }
        //tempAlbum.setCover(coverFile.getAbsolutePath());
        if (!"".equals(eanField.getText())) {
            tempAlbum.setEAN(eanField.getText());
        }
        if (!"".equals(genreField.getText())) {
            tempAlbum.setGenre(genreField.getText());
        }
        if (!"".equals(interpretField.getText())) {
            tempAlbum.setInterpret(interpretField.getText());
        }
        if (!"".equals(albumField.getText())) {
            tempAlbum.setName(albumField.getText());
        }

        //tempAlbum.setRating(ratingChoiceBox.getSelectionModel().getSelectedIndex() + 1);
        for (Integer i = 0; i < newTitles.size(); i++) {
            cdorgTitles.add(new CDOrgTitle(newTitles.get(i).getIndex(), newTitles.get(i).getName()));
        }
        tempAlbum.setTitles(cdorgTitles);
        /*tempAlbum.setCompilation(compilationCheckBox.isSelected());
         tempAlbum.setCD(cdCheckBox.isSelected());
         tempAlbum.setTape(tapeCheckBox.isSelected());
         tempAlbum.setVinyl(vinylCheckBox.isSelected());
         tempAlbum.setDigital(digitalCheckBox.isSelected());*/
        //testcomp = new CDOrgAlbumComponent(localDatabaseScrollAnchor, tempAlbum, 10, 10);

        //cdorgAlbums = discogsInterface.discogsRequest(tempAlbum, 50);


    }

    @FXML
    private void handleAddTitleButtonAction(ActionEvent event) {
        CDOrgTitle tempTitle = new CDOrgTitle(newTitles.size() + 1, insertTitleField.getText());
        insertTitleField.clear();
        newTitles.add(new CDOrgNewTitleComponent(tempTitle, titlesPane, (110 + newTitles.size() * 25), 14, newTitles.size()));
        deleteEvents.add(new titleDeleteEvent(newTitles.size() - 1, newTitles, titlesPane, this));

        newTitles.get(newTitles.size() - 1).getDeleteButton().setOnAction(deleteEvents.get(deleteEvents.size() - 1).getEvent());
        if (newTitles.get(newTitles.size() - 1).getTextField().getLayoutY() + 22 > titlesPane.getHeight()) {
            titlesPane.setPrefHeight(titlesPane.getHeight() + 25);
        }
    }

    @FXML
    private void handleMenuClose(ActionEvent event) {
        System.exit(0);
    }

    @FXML
    private void handleCoverSearch(ActionEvent event) {
        final JFileChooser fc = new JFileChooser();
        fc.addChoosableFileFilter(new FileNameExtensionFilter("Bilddateien", "jpg", "png", "bmp"));
        fc.setAcceptAllFileFilterUsed(false);  //Entfernt den "Alle Dateien" Modus
        int returnVal = fc.showOpenDialog(null);

        if (returnVal == JFileChooser.APPROVE_OPTION) {
            coverFile = fc.getSelectedFile();
            try {
                BufferedImage bufimg = ImageIO.read(coverFile);
                Image cover = SwingFXUtils.toFXImage(bufimg, null);
                coverImage.setImage(cover);
            } catch (IOException ex) {
            }
        }
//        System.out.println(coverFile.getAbsolutePath());
    }

    @FXML
    private void handleDeleteButton(ActionEvent e) {
        System.out.println(System.getProperty("user.dir"));
    }

    @FXML
    private void handleEditCheckbox(ActionEvent e) {
        if (editCheckBox.isSelected()) {
            eanField.setDisable(false);
            albumField.setDisable(false);
            insertTitleField.setDisable(false);
            interpretField.setDisable(false);
            genreField.setDisable(false);
            yearField.setDisable(false);
            ratingChoiceBox.setDisable(false);
            searchCoverButton.setDisable(false);
            compilationCheckBox.setDisable(false);
            cdCheckBox.setDisable(false);
            tapeCheckBox.setDisable(false);
            vinylCheckBox.setDisable(false);
            digitalCheckBox.setDisable(false);
            deleteButton.setDisable(false);
            acceptButton.setDisable(false);
        } else {
            eanField.setDisable(true);
            albumField.setDisable(true);
            insertTitleField.setDisable(true);
            interpretField.setDisable(true);
            genreField.setDisable(true);
            yearField.setDisable(true);
            ratingChoiceBox.setDisable(true);
            searchCoverButton.setDisable(true);
            compilationCheckBox.setDisable(true);
            cdCheckBox.setDisable(true);
            tapeCheckBox.setDisable(true);
            vinylCheckBox.setDisable(true);
            digitalCheckBox.setDisable(true);
            deleteButton.setDisable(true);
            acceptButton.setDisable(true);
        }
    }

    public void handleMinimizeMultiMenuButton(ActionEvent e) {
        //"einklappen" des menus setzt alle Objekte (bis auf den einklapp Button)
        //im rechten Pane auf visible(false) und färbt das was übrig bleibt grau
        //--Michael
        if (menuIsMinimized) {
            menuIsMinimized = false;
            minimizeMultiMenu.setText(">>");
            mainSplitPane.setDividerPosition(0, idealRatio);
            menuToolbar.setVisible(true);
            rightAlbumInsert.setVisible(true);
            rightAlbumProperties.setVisible(true);
            acceptButton.setVisible(true);
            deleteButton.setVisible(true);
            rightAlbumInfoBackground.setStyle("-fx-background-color: rgb(255,255,255);");
            rightAlbumInfo.setStyle("-fx-background-color: rgb(255,255,255);");
        } else {
            menuIsMinimized = true;
            minimizeMultiMenu.setText("<<");
            mainSplitPane.setDividerPosition(0, 0.973);
            menuToolbar.setVisible(false);
            rightAlbumInsert.setVisible(false);
            rightAlbumProperties.setVisible(false);
            acceptButton.setVisible(false);
            deleteButton.setVisible(false);
            rightAlbumInfoBackground.setStyle("-fx-background-color: rgb(166,166,166);");
            rightAlbumInfo.setStyle("-fx-background-color: rgb(166,166,166);");
    
    

        }
    }

    public void deleteTempTitles() {
        for (Integer i = 0; i < newTitles.size(); i++) {
            newTitles.get(i).delete();
        }
        newTitles.clear();
        cdorgTitles.clear();
        deleteEvents.clear();
    }

    private void checkTitleFields() {
        for (Integer i = 0; i > newTitles.size(); i++) {
            if (newTitles.get(i).isDeleted()) {
                newTitles.remove(i);
            }
        }
    }

    private void showSearchResults() throws IOException {
        for (int i = 0; i < searchedAlbums.size(); i++) {
            albumcomponents.add(new CDOrgAlbumComponent(localDatabaseScrollAnchor, searchedAlbums.get(i), 10 + (albumcomponents.size() * 135), 10, albumcomponents.size()));
            albumEvents.add(new albumExpandEvent(albumcomponents.size() - 1, albumcomponents, localDatabaseScrollAnchor, this));
            albumcomponents.get(albumcomponents.size() - 1).getExpandButton().setOnAction(albumEvents.get(albumEvents.size() - 1).getExpandEvent());
            albumcomponents.get(albumcomponents.size() - 1).getCoverimage().setOnMousePressed(albumEvents.get(albumEvents.size() - 1).getClickEvent());
        }
    }

    /* private boolean checkSimilar(CDOrgAlbum alb) {
     //TODO
     //Popup Warnung falls doppelt mit auswahl fortfahren/abbrechen
     return false;

     }*/
    //getter für einige der Objekte auf der Oberfläche um dynamisch auf events reagieren zu können
    public TextField getEANField() {
        return eanField;
    }

    public TextField getAlbumField() {
        return albumField;
    }

    public TextField getInterpretField() {
        return interpretField;
    }

    public TextField getGenreField() {
        return genreField;
    }

    public TextField getYearField() {
        return yearField;
    }

    public CheckBox getCDCheckBox() {
        return cdCheckBox;
    }

    public CheckBox getTapeCheckBox() {
        return tapeCheckBox;
    }

    public CheckBox getVinylCheckBox() {
        return vinylCheckBox;
    }

    public CheckBox getCompilationCheckBox() {
        return compilationCheckBox;
    }

    public ChoiceBox getRatingChoiceBox() {
        return ratingChoiceBox;
    }

    public CheckBox getDigitalCheckBox() {
        return digitalCheckBox;
    }

    public ImageView getCoverImageView() {
        return coverImage;
    }

    public CheckBox getEditCheckBox() {
        return editCheckBox;
    }

    public Button getSearchButton() {
        return searchButton;
    }

    public Button getInsertButton() {
        return insertButton;
    }

    public Integer getSelectedindex() {
        return currentSelectedIndex;
    }

    public ArrayList<CDOrgAlbum> getSearchedAlbums() {
        return searchedAlbums;
    }

    public void setSearchedAlbums(ArrayList<CDOrgAlbum> albums) throws IOException {
        searchedAlbums = albums;
        showSearchResults();
    }

    public void setCurrentIndex(Integer n) {
        currentSelectedIndex = n;
    }

    public void refreshEditCheckBox(Boolean state) {
        editCheckBox.setSelected(state);
        handleEditCheckbox(new ActionEvent());
    }

    public void showAlbumTitles(Integer index) {
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        newTitles = new ArrayList<>();
        deleteEvents = new ArrayList<>();
        albumcomponents = new ArrayList<>();
        albumEvents = new ArrayList<>();
        searchedAlbums = new ArrayList<>();
        cdorgTitles = new ArrayList<>();
        tempAlbum = new CDOrgAlbum();
        //discogsInterface = new DiscogsInterface();
        discogsSearchThread = new DiscogsSearchThread(tempAlbum, false, this);
        //discogsSearchThread.start();
        searchCheck = new SearchCheckerThread();
        searchCheck.start();
        
        currentSelectedIndex = -1;//steht für keine Auswahl
        deleteButton.setDisable(true);
        searchButton.setDefaultButton(true);
        insertButton.setDefaultButton(false);
        searchProgress.setVisible(false);
        menuIsMinimized = false;
        editCheckBox.setDisable(true);


        //Position des Trenners ideal berechnen anhand des Bildschirms des Verwednders
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();
        idealRatio = 1 - (350 / bounds.getWidth());
        mainSplitPane.setDividerPosition(0, idealRatio);//!!!tut nix anscheinend existiert der splitter hier noch nicht

        //Items für die SearchTypeChoiceBox einfügen
        searchTypeChoice.setItems(FXCollections.observableArrayList("Alle Titel", "min. 1 Titel"));
        searchTypeChoice.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> ov, Number t, Number t1) {
                if (t1.intValue() == 0) {
                    //TODO
                    //Alle Titel suchen
                } else if (t1.intValue() == 1) {
                    //TODO
                    //min 1 Titel suchen
                }
            }
        });
        
       

        //Items der ChoiceBox für das Rating initialisieren
        ratingChoiceBox.setItems(FXCollections.observableArrayList("1", "2", "3", "4", "5", "6", "7", "8", "9", "10"));
        //Listener für das Klicken auf die einzelnen Elemente der ChoiceBox
        ratingChoiceBox.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> ov, Number t, Number t1) {
                tempAlbum.setRating(t1.intValue() + 1);
            }
        });

        insertTitleField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (newValue == "") {
                    addTitleButton.setDisable(true);
                } else {
                    addTitleButton.setDisable(false);
                }
            }
        });

        interpretField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                if (!t.equals(t1) && t1.length() >= 4) {
                    searchCheck.checkInput(t1);
                }
            }
        });

        eanField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String t, String t1) {
            }
        });

        //mainSplitPane.styleProperty().setValue("");
    }
}
