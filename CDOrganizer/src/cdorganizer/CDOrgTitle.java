/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cdorganizer;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author Dennis
 */
public class CDOrgTitle {
    private final SimpleStringProperty titleName;
    private final SimpleStringProperty titlePath;
    private final SimpleDoubleProperty titleLength;
    private final SimpleIntegerProperty titleNr;
    
    public CDOrgTitle(Integer nr, Double titlelength, String titlename, String path) {       
        this.titleLength = new SimpleDoubleProperty(titlelength);
        this.titleName = new SimpleStringProperty(titlename);
        this.titlePath = new SimpleStringProperty(path);
        this.titleNr = new SimpleIntegerProperty(nr);
    }
    public CDOrgTitle(Integer nr, String titlename) {
        this.titleLength = new SimpleDoubleProperty(0.0);
        this.titleName = new SimpleStringProperty(titlename);
        this.titlePath = new SimpleStringProperty("");
        this.titleNr = new SimpleIntegerProperty(nr);
    }

    public Double getTitleLength(){
        return titleLength.get();
    }
    public SimpleDoubleProperty getTitleLengthProperty(){
        return titleLength;
    }
    public void setTitlelength(Double titlelength) {
        this.titleLength.set(titlelength);
    }

    public String getTitleName() {
        return titleName.get();
    }
    public void setTitlename(String titlename) {
        this.titleName.set(titlename);
    }
    public SimpleStringProperty getTitleNameProperty(){
        return titleName;
    }

    public String getTitlepath() {
        return titlePath.get();
    }
    public void setTitlepath(String titlepath) {
        this.titlePath.set(titlepath);
    }
    public SimpleStringProperty getTitlePathProperty(){
        return titlePath;
    }
    
    public Integer getTitleNr(){
        return titleNr.get();
    }
    public void setTitleNr(Integer nr){
        this.titleNr.set(nr);
    }
    public SimpleIntegerProperty getTitleNrProperty(){
        return titleNr;
    }      
    
}
