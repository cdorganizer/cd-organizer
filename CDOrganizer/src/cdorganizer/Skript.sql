create table Interpret(
	IntID integer primary key autoincrement,
	IntName varchar(255) not null unique default 'unknown'
	);

create table Medium(
	MedID integer primary key autoincrement,
	MedTyp varchar(7) unique
	);

create table Genre(
	GenID integer primary key autoincrement,
	GenName varchar(255) unique
	);


create table Titel(
	TitID integer primary key autoincrement,
	TitName varchar(255) not null
	);

create table Album(
	AlbID integer primary key autoincrement,
	EanNr integer
	MedID integer references Medium (MedID) on update cascade on delete set null,
	AlbName varchar(255) not null,
	AlbInterpret integer not null references Interpret (IntID) on update cascade on delete cascade,
	GenID integer references Genre (GenID) on update cascade on delete set null,
	AlbRating integer,
	AlbCover varchar(255) default '\pics\no-icon.png'	
	);

create table AlbumTitel(
	AlbID integer references Album (AlbID) on update cascade on delete cascade,
	TitID integer references Titel (TitID) on update cascade on delete cascade,
	primary key(AlbID,TitID)
	);

insert into Interpret (IntName) values ('Various Artists');
insert into Medium (MedTyp) values ('CD'),('Vinyl'),('digital'),('Tape');
insert into Genre (GenName) values ('Rock'),('Metal');
insert into Interpret (IntName) values ('Metallica'),('Die Ärzte');
insert into Titel (TitName) values ('Testtitel1'),('Testtitel2');
insert into Album (AlbName,AlbInterpret,GenID,MedID) values
('Metallica 1',
	(select IntID from Interpret where IntName='Metallica'),
	(select GenID from Genre where GenName='Metal'),
	(select MedID from Medium where MedTyp='CD')),
('Ärzte 1',
	(select IntID from Interpret where IntName='Die Ärzte'),
	(select GenID from Genre where GenName='Rock'),
	(select MedID from Medium where MedTyp='CD'));
	
insert into AlbumTitel (AlbID,TitID) values 
(
	(select AlbID from Album where AlbName='Metallica 1'),
	(select TitID from Titel where TitName='Testtitel1')),
(
	(select AlbID from Album where AlbName='Metallica 1'),
	(select TitID from Titel where TitName='Testtitel2')),
(
	(select AlbID from Album where AlbName='Ärzte 1'),
	(select TitID from Titel where TitName='Testtitel1')),
(
	(select AlbID from Album where AlbName='Ärzte 1'),
	(select TitID from Titel where TitName='Testtitel2'));
	
	

