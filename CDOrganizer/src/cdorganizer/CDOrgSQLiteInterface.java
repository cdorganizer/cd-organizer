/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cdorganizer;

import java.util.ArrayList;
import java.sql.*;
import java.io.File;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ab
 */
public class CDOrgSQLiteInterface {
    private String path;
    private Connection c;
    
    public CDOrgSQLiteInterface(){
        super();
        
    }
    /**
     * readFile(String path) reads and returns a textfile, which is located at
     * "path"
     * This function is used by OpenDB, to load a sql-Script for creating the
     * SQLite-Database
     * @param path the path where the textfile, which has to be read, is located
     * @return readFile returns a String, which contains the content of the text-file given by "path"
     * @throws IOException if file can't be read
     */
    private String readFile(String path) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(path));
        try {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine(); //read first line

            while (line != null) {     //iterate all lines
                sb.append(line);      // write line into StringBuilder
                sb.append("\n");     // add a line break
                line = br.readLine(); // read next line
            }
            return sb.toString();
        } finally {
            br.close();
        }
    }
    
    /**
     * openDB opens the SQLite-File which is located at "path"
     * if this file does not exist, it creates a new SQLite-File
     * >> function makes status-posts in console
     * @param path the path where the SQLite-File is located
     */
    public void openDB(String path) {
        this.path = path;
        c = null;
        try {
            Class.forName("org.sqlite.JDBC"); 
            File f = new File(path);
            if(f.exists() && !f.isDirectory()) {
                c = DriverManager.getConnection("jdbc:sqlite:"+path); //open connection to existing SQLite-File
                System.out.println("database does already exist");
            } else {
                c = DriverManager.getConnection("jdbc:sqlite:"+path); //create connection to new SQLite-File
                sqlQuery(readFile("C:\\Users\\ab\\Downloads\\SOFTWT2\\CDOrganizer\\src\\cdorganizer\\S.sql")); //create tables
                System.out.println("created database successfully");
            }
            System.out.println("openend database successfully");
        } catch (Exception e) {
            System.out.println(e.getClass().getName() +  " : " + e.getMessage());
        }
        
    }
    
    /**
     * saveDB closes the connection c
     * TODO : copy SQLiteFile if path differs from this.path
     * @param path 
     */
    public void saveDB(String path) {
        try {
            c.close();
        } catch (SQLException e) {
            System.err.println(e.getClass().getName() + " : " + e.getMessage());
        }
    }
    
    public ArrayList<CDOrgAlbum> getDB() {
        ArrayList<CDOrgAlbum> tempDB =  new ArrayList<CDOrgAlbum>();
        return tempDB;
    }
    
    
    /**
     * Inserts a new CD into the SQLite-Database, which is given by the the CDOrgAlbum-Parameter CD 
     * >> function makes status-posts in console
     * TODO : PopUp, etc..
     * @param CD CDOrgAlbum, which is inserted into the database
     */
    public void addCDOrgAlbum(CDOrgAlbum CD) {
        String sql;
        
        //--- Genre ----
        int GenID;
        sql = "Select * from Genre where GenName = '" + CD.getGenre() + "';";
        ResultSet rs = sqlQuery(sql);
        try {
            rs.next();
            GenID = rs.getInt("GenID"); //if this fails, this Genre is not in the database yet 
            System.out.println("Genre " + CD.getGenre() + " already in database");
        } catch (SQLException e) { //Genre not in Database
            sql = "insert into Genre (GenName) values ('" + CD.getGenre() + "');";
            sqlQuery(sql);
            System.out.println("added Genre " + CD.getGenre() + " successfully");
        };
        
        //--- Medium ----
        int MedID;
        sql = "Select * from Medium where MedTyp = '" + CD.getMedium() + "';";
        rs = sqlQuery(sql);
        try {
            rs.next();
            MedID = rs.getInt("MedID"); //if this fails, this Medium is not in the database yet  
            System.out.println("Medium " + CD.getMedium() + "already in database");
        } catch (SQLException e) { //Medium not in Database
            sql = "insert into Medium (MedTyp) values ('" + CD.getMedium() + "');";
            sqlQuery(sql);
            System.out.println("added Medium " + CD.getMedium() + " succesfully");
        };
        
        //--- Interpret ----
        int IntID;
        sql = "Select * from Interpret where IntName = '" + CD.getInterpret() + "';";
        rs = sqlQuery(sql);
        try {
            rs.next();
            IntID = rs.getInt("IntID"); //if this fails, this Interpret is not in the database yet 
            System.out.println("Interpret " + CD.getInterpret() + " already in database");
        } catch (SQLException e) { //Interpret not in Database
            sql = "insert into Interpret (IntName) values ('" + CD.getInterpret() + "');";
            sqlQuery(sql);
            System.out.println("added Interpret " + CD.getInterpret() + " successfully");
        };
        
        //--- Album ----
        //getting the GenID, which is needed as foreignkey at AlbumTable
        sql = "Select * from Genre where GenName = '" + CD.getGenre() + "';";
        rs = sqlQuery(sql);
        GenID = 0;
        try {
            rs.next();
            GenID = rs.getInt("GenID"); //get primary key of searched Genre
        } catch (SQLException e) { //Genre not in database
            System.err.println(e.getClass().getName() + " : " + e.getMessage());
        };
        
        //getting the MedID, which is needed as foreignkey at AlbumTable
        sql = "Select * from Medium where MedTyp = '" + CD.getMedium() + "';";
        rs = sqlQuery(sql);
        MedID = 0;
        try {
            rs.next();
            MedID = rs.getInt("MedID");  //get primary key of searched Medium
        } catch (SQLException e) { //Medium not in Database
            System.err.println(e.getClass().getName() + " : " + e.getMessage());
        };
        
        //getting the IntID, which is needed as foreignkey at AlbumTable
        sql = "Select * from Interpret where IntName = '" + CD.getInterpret() + "';";
        rs = sqlQuery(sql);
        IntID = 0;
        try {
            rs.next();
            IntID = rs.getInt("IntID"); //get primary key of searched Interpret 
        } catch (SQLException e) { //Interpret not in Database
            System.err.println(e.getClass().getName() + " : " + e.getMessage());
        };
        
        //inserting Album
        int AlbumID = 0;
        sql = "Select * from Album where " + 
                "EanNr = '" + CD.getEAN() + "' and " +
                "MedID = " + MedID + " and " +
                "AlbName = '" + CD.getName() + "' and " +
                "AlbInterpret = " + IntID + " and " +
                "GenID = " + GenID + " and " +
                "AlbRating = " + CD.getRating().toString() + " and " +
                "AlbCover = '" + CD.getCover() + "';";
        rs = sqlQuery(sql);
        try {
            rs.next();
            AlbumID = rs.getInt("AlbID"); //if this fails, this Album is not in the database yet 
            System.out.println("Album " + CD.getName() + " already in database");
        } catch (SQLException e) { //Album not in Database
            sql = "insert into Album (EanNr,MedID,AlbName,AlbInterpret,GenID,AlbRating,AlbCover) " +
                     " values ('" + CD.getEAN() + "', " + MedID + ", '" +
                                    CD.getName() + "', " + IntID + ", " +
                                    GenID + ", " + CD.getRating().toString() + ", '" + 
                                    CD.getCover() +"');"; 
            sqlQuery(sql);
            System.out.println("added Album " + CD.getName() + " successfully");
        };
        
        //Titles and AlbumTitles
        for (int i = 0; i < CD.getTitleCount(); i++){
            CDOrgTitle Title = CD.getTitle(i);
           
            //--- Title ----
            sql = "Select * from Titel where TitName = '" + Title.getTitleName()+
                    "' and TitLen = " + Title.getTitleLength().toString() + ";";
            rs = sqlQuery(sql);
            int TitID = 0;
            try {
                rs.next();
                TitID = rs.getInt("TitID"); //if this fails, this Title is not in the database yet 
                System.out.println("Title " + Title.getTitleName() + " already in database");
            } catch (SQLException e) { //Title not in Database
                sql = "insert into Titel (TitName, TitLen) values ('" +
                      Title.getTitleName() + "', " + Title.getTitleLength().toString() +  ");";
                sqlQuery(sql);
                System.out.println("added Title " + Title.getTitleName() + " successfully");
            };
            
            //--- AlbumTitle ----
            //getting Titid, which is needed as foreignkey at AlbumTitleTable
            sql = "Select * from Titel where TitName = '" + Title.getTitleName()+
                    "' and TitLen = " + Title.getTitleLength().toString() + ";";
            rs = sqlQuery(sql);
            try {
                rs.next();
                TitID = rs.getInt("TitID"); //get primary key of searched Title
            } catch (SQLException e) {
                System.err.println(e.getClass().getName() + " : "+ e.getMessage());
            };
            
            //getting AlbumID, which is needed as foreignkey at AlbumTitleTable
            sql = "Select * from Album where " + 
                    "EanNr = '" + CD.getEAN() + "' and " +
                    "MedID = " + MedID + " and " +
                    "AlbName = '" + CD.getName() + "' and " +
                    "AlbInterpret = " + IntID + " and " +
                    "GenID = " + GenID + " and " +
                    "AlbRating = " + CD.getRating().toString() + " and " +
                    "AlbCover = '" + CD.getCover() + "';";
            rs = sqlQuery(sql);
            try {
                rs.next();
                AlbumID = rs.getInt("AlbID"); //get primary key of searched Album
            } catch (SQLException e) { 
                System.err.println(e.getClass().getName() + " : " + e.getMessage());
            };
            
            //inserting AlbumTitle
            sql = "Select * from AlbumTitel where albID = " + AlbumID + " and  titID = " + TitID + ";";
            rs = sqlQuery(sql);
            try {
                rs.next();
                int temp = rs.getInt("AlbID"); //if this fails AlbumTitle is not in database yet
                System.out.println("AlbumTitel (" + AlbumID + ", " + TitID + ") already in database");  
            } catch (SQLException e) { //AlbumTitle not in database
                sql = "insert into AlbumTitel (albID, titID) values (" +
                      AlbumID + ", " + TitID +  ");";
                sqlQuery(sql);
                System.out.println("added AlbumTitle " + Title.getTitleName() + " successfully");
            }
            
        }
    }
    
    
    public void changeCDOrgAlbum(CDOrgAlbum CD) {
    
    }
    
    public void deleteCDOrgAlbum(CDOrgAlbum CD) {

    }
    
    public ArrayList<CDOrgAlbum> searchCDOrgAlbum(CDOrgAlbum CD) {
        ArrayList<CDOrgAlbum> tempSearchResults = new ArrayList<CDOrgAlbum>();
        String sql = "select * from Album where ";
        boolean andNeeded = false;
        
        //checking for which CDOrgAlbum-Attributes has to be searched
        //if their value is default-value they won't be included in the search
        
        if (!CD.getEAN().equals("")) {
            sql += "eanNr = '" + CD.getEAN() + "'";
            andNeeded = true;
        }
        
        if (!CD.getMedium().equals("")) {
            if (andNeeded) {
                sql += " and ";
            }
            sql += "MedID = (Select MedID from Medium where MedTyp = '" + CD.getMedium() + "')";
            andNeeded = true;
        }
        
        if (!CD.getName().equals("")) {
            if (andNeeded) {
                sql += " and ";
            }
            sql += "AlbName = '" +  CD.getName() + "'";
            andNeeded = true;
        }
        
        if (!CD.getInterpret().equals("")) {
            if (andNeeded) {
                sql += " and ";
            }
            sql += "AlbInterpret = (Select IntID from Interpret where IntName = '" + CD.getInterpret() + "')";
            andNeeded = true;
        }
        
        if (!CD.getGenre().equals("")) {
            if (andNeeded) {
                sql += " and ";
            }
            sql += "GenID = (Select GenID from Genre where GenName = '" + CD.getGenre() + "')";
            andNeeded = true;
        }
        
        sql += ";";
        ResultSet rs = sqlQuery(sql);
        try {
            while (rs.next()) {
                System.out.println("found Album " + rs.getString("AlbName"));
            }
        } catch (SQLException e) {
            System.err.println(e.getClass().getName() + " : " + e.getMessage());
        }   
        
        return tempSearchResults;
    }
    
    /**
     * Executes the given SQLite-Query "query" on the SQLite-Database, which 
     * before hast to be opened. If "query" contains no "Select", the returned 
     * ResultSet will be null.
     * @param query A Select-query always has to start with an capital S >>'Select * From ... '
     * @param autoCommit must be true for a Select-query
     * @return returns ResultSet, which contains all results, code-example : ResultSet rs = sqlQuery(...); while (rs.next()) {int i = rs.getInt("AlbumID"); String s = rs.getString("AlbumName");}
     */
    private ResultSet sqlQuery(String query) {
        Statement stmt = null;
        ResultSet rs = null;        
        try {
          stmt = c.createStatement();
          if (query.contains("Select")) {
              rs = stmt.executeQuery(query);
              //stmt is not closed here, for not not to lose the value of "rs"
          } else {
              stmt.executeUpdate(query);
              stmt.close();
          }
        } catch ( Exception e ) {
          System.err.println(e.getClass().getName() + " : " + e.getMessage());
        }
        return rs;
    }
    
    
    
    
    
}
