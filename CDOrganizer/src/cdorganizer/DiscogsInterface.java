/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cdorganizer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import org.json.JSONException;

/**
 *
 * @author ab
 */
public class DiscogsInterface {

    private int searchItemIndex = 0;
    private int maxItems;
    private org.json.JSONArray savedResults;

    public void DiscogsInterface() {
        //Constructor
    }

    /**
     * public function which is called in the main programm to make a request to
     * the online-database discogs
     *
     * @param Request CDOrgAlbum-Object, which contains all the informations the
     * user is asking for
     * @param resultsCount valid values : 1-1000
     * @return returns a ArrayList which contains all search-results as
     * CDOrgAlbum-Objects
     * @throws org.json.JSONException
     * @throws java.io.IOException
     */
    public ArrayList<CDOrgAlbum> discogsRequest(CDOrgAlbum Request, int resultsCount) throws JSONException, IOException {
        ArrayList<CDOrgAlbum> CDOrgAlbumResults = new ArrayList<CDOrgAlbum>();

        if (Request.getEAN().equals("")) { //normal search, without EAN
            String request = "http://api.discogs.com/database/search?q=" + CDOrgAlbumToRequestString(Request) + "&q&per_page=" + resultsCount + "&page=1";
            org.json.JSONObject jsonObj = httpRequest(request);
            CDOrgAlbumResults = jsonObjectToCDOrgAlbumResults(jsonObj);
            return CDOrgAlbumResults;
        } else {  //search with EAN - number
            String request = "http://api.discogs.com/database/search?q=" + Request.getEAN() + "&barcode&per_page=" + resultsCount + "&page=1";
            org.json.JSONObject jsonObj = httpRequest(request);
            CDOrgAlbumResults = jsonObjectToCDOrgAlbumResults(jsonObj);
            return CDOrgAlbumResults;
        }

    }
    /*
     * Holt alle nötigen daten für eine Abfrage und speichert das resultierende
     * JSONArray, dient zur Vorbereitung der Methode "findNext"
     */
    public void singleRequest(CDOrgAlbum request, int resultsCount) throws JSONException, IOException {
        maxItems = resultsCount;
        org.json.JSONObject jsonObj;
        if (request.getEAN().equals("")) {
            String requestStr = "http://api.discogs.com/database/search?q=" + CDOrgAlbumToRequestString(request) + "&q+per_page=" + resultsCount + "&page=1";
            jsonObj = httpRequest(requestStr);
            savedResults = jsonObj.getJSONArray("results");
        } else {
            String requestStr = "http://api.discogs.com/database/search?q=" + request.getEAN() + "&barcode&per_page=" + resultsCount + "&page=1";
            jsonObj = httpRequest(requestStr);
            savedResults = jsonObj.getJSONArray("results");
        }
        //return findNext();
    }
    
    /*
     * Die Methode "findNext" gibt nur das jeweils nächste suchergebniss des in
     * singleRequest angegebenen Suchparameters aus, sobald mehr suchergebnisse 
     * abgefragt werden, als vorhanden sind wird eine Exception geworfen
     */
    public CDOrgAlbum findNext() throws JSONException, IOException {
        CDOrgAlbum tempCDOrgAlbum = new CDOrgAlbum();
        org.json.JSONArray tempJsonArr;
        org.json.JSONObject tempJsonObj;
        org.json.JSONObject jsonObj = savedResults.getJSONObject(searchItemIndex);
        System.out.println(jsonObj.getString("type"));

        if (jsonObj.getString("type").equals("release")) {
            //tempCDOrgAlbum = new CDOrgAlbum();

            //EAN
            try {
                tempCDOrgAlbum.setEAN(jsonObj.getString("catno"));
            } catch (JSONException e) {
                tempCDOrgAlbum.setEAN("");
            }

            //Name
            try {
                jsonObj = httpRequest(jsonObj.getString("resource_url"));
                tempCDOrgAlbum.setName(jsonObj.getString("title"));
            } catch (JSONException e) {
                tempCDOrgAlbum.setName("failure");
            }

            //Genre
            try {
                tempJsonArr = jsonObj.getJSONArray("styles");
                tempCDOrgAlbum.setGenre(tempJsonArr.getString(0));
            } catch (JSONException e) {
            }

            //Interpret
            try {
                tempJsonArr = jsonObj.getJSONArray("artists");
                tempJsonObj = (org.json.JSONObject) tempJsonArr.get(0);
                tempCDOrgAlbum.setInterpret(tempJsonObj.getString("name"));
            } catch (JSONException e) {
                tempCDOrgAlbum.setInterpret("failure");
            }

            //ALL Titles at once
            try {
                tempCDOrgAlbum.setTitles(jsonObjectToCDOrgTitles(jsonObj.getJSONArray("tracklist")));
            } catch (JSONException e) {
            }

            //Cover
            try {
                tempJsonArr = jsonObj.getJSONArray("images");
                tempJsonObj = tempJsonArr.getJSONObject(0);
                tempCDOrgAlbum.setCover(tempJsonObj.getString("uri")); //uri == url of the image
            } catch (JSONException e) {
            }

            //Year
            try {
                tempCDOrgAlbum.setYear(jsonObj.getInt("year"));
            } catch (JSONException e) {
            }

            //IsCompilation
            tempCDOrgAlbum.setCompilation(tempCDOrgAlbum.getInterpret().equals("Various"));

            System.out.println(tempCDOrgAlbum.toString());

            if (searchItemIndex > savedResults.length()) {
                throw new IOException("ItemIndex exceeded maxitem Count");
            }
        }
        searchItemIndex++;
        return tempCDOrgAlbum;
    }

    /**
     * does an httpRequest of the URL "url", receives the answer, which is
     * formatted as a JSONObject private function which is called by
     * discogsRequest(). It makes a httpRequest, records and saves the answer of
     * the server in a JSONObject
     *
     * @param url URL, which is requested
     * @return returns the answer of the httpRequest, formatted as a JSONObject
     */
    private org.json.JSONObject httpRequest(String url) throws IOException, JSONException {

        URL obj = new URL(url);

        // http : Header :
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("GET");
        con.setRequestProperty("User-Agent", "cdorganizer/0.1");

        // sending Request
        int responseCode = con.getResponseCode();

        // print http-respons
        System.out.println("\n Sending 'GET' request to URL : " + url);
        System.out.println("Response Code : " + responseCode);

        //receiving Request
        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        //Convert to json
        org.json.JSONObject jsonObj = new org.json.JSONObject(response.toString());
        return jsonObj;

    }

    /**
     * private function which is called by discogsRequest, after httpRequest to
     * transfer the JSONObject into one CDOrgAlbum-Object per result
     *
     * @param AllResults a JSONObject which contains all searchResults you got
     * by doing httpRequest()
     * @return returns all search-results formatted as a ArrayList of CDOrgAlbum
     * @throws JSONException
     * @throws IOException
     */
    private ArrayList<CDOrgAlbum> jsonObjectToCDOrgAlbumResults(org.json.JSONObject AllResults) throws JSONException, IOException {
        ArrayList<CDOrgAlbum> TempCDOrgAlbumResults = new ArrayList<CDOrgAlbum>();
        CDOrgAlbum TempCDOrgAlbum;

        org.json.JSONArray jsonArray = AllResults.getJSONArray("results");
        org.json.JSONArray tempJsonArr;
        org.json.JSONObject tempJsonObj;
        for (int i = 0; i < jsonArray.length(); i++) { //iterates all the results
            org.json.JSONObject jsonObj = jsonArray.getJSONObject(i);
            System.out.println(jsonObj.getString("type"));
            if (jsonObj.getString("type").equals("release")) {
                TempCDOrgAlbum = new CDOrgAlbum();

                //EAN
                try {
                    TempCDOrgAlbum.setEAN(jsonObj.getString("catno"));
                } catch (JSONException e) {
                }

                //Name
                try {
                    jsonObj = httpRequest(jsonObj.getString("resource_url"));
                    TempCDOrgAlbum.setName(jsonObj.getString("title"));
                } catch (JSONException e) {
                }

                //Genre
                try {
                    tempJsonArr = jsonObj.getJSONArray("styles");
                    TempCDOrgAlbum.setGenre(tempJsonArr.getString(0));
                } catch (JSONException e) {
                }

                //Interpret
                try {
                    tempJsonArr = jsonObj.getJSONArray("artists");
                    tempJsonObj = (org.json.JSONObject) tempJsonArr.get(0);
                    TempCDOrgAlbum.setInterpret(tempJsonObj.getString("name"));
                } catch (JSONException e) {
                }

                //ALL Titles at once
                try {
                    TempCDOrgAlbum.setTitles(jsonObjectToCDOrgTitles(jsonObj.getJSONArray("tracklist")));
                } catch (JSONException e) {
                }

                //Cover
                try {
                    tempJsonArr = jsonObj.getJSONArray("images");
                    tempJsonObj = tempJsonArr.getJSONObject(0);
                    TempCDOrgAlbum.setCover(tempJsonObj.getString("uri")); //uri == url of the image
                } catch (JSONException e) {
                }

                //Year
                try {
                    TempCDOrgAlbum.setYear(jsonObj.getInt("year"));
                } catch (JSONException e) {
                }

                //IsCompilation
                TempCDOrgAlbum.setCompilation(TempCDOrgAlbum.getInterpret().equals("Various"));


                TempCDOrgAlbumResults.add(TempCDOrgAlbum); //adds this result to the arraylist
                //TODO remove
                System.out.println(TempCDOrgAlbum.toString());
            }
        }
        return TempCDOrgAlbumResults;
    }

    /**
     * private function which is called by jsonObjectToCDOrgAlbumResults() to
     * convert the tracklist, formatted as a jsonArray into a ArrayList of
     * CDOrgTitle
     *
     * @param jsonArray a JSONArray which contains all titles of a release
     * @return returns all titles of a release, formatted as a ArrayList of
     * CDOrgTitle
     * @throws JSONException
     */
    private ArrayList<CDOrgTitle> jsonObjectToCDOrgTitles(org.json.JSONArray jsonArray) throws JSONException {
        ArrayList<CDOrgTitle> TempCDOrgTitles = new ArrayList<CDOrgTitle>();
        CDOrgTitle TempCDOrgTitle;
        for (int i = 0; i < jsonArray.length(); i++) { //iterates all titles
            //get one title
            org.json.JSONObject jsonObj = jsonArray.getJSONObject(i);

            //title-name
            String titleName = jsonObj.getString("title");

            //length /duration
            String tempStr = jsonObj.getString("duration");
            double titleLength = 0.0;
            if (!tempStr.equals("")) {
                titleLength = Double.parseDouble(tempStr.replace(":", "."));
            }
            TempCDOrgTitle = new CDOrgTitle(i, titleLength, titleName, "");

            //add title to array list
            TempCDOrgTitles.add(TempCDOrgTitle);
        }
        return TempCDOrgTitles;
    }

    /**
     * @param CDOrgAlb
     * @return returns a String which contains Interpret, Name, Year, Genre and
     * Medium
     */
    private String CDOrgAlbumToRequestString(CDOrgAlbum CDOrgAlb) {
        String TempStr = CDOrgAlb.getInterpret() + " " + CDOrgAlb.getName() + " ";
        if (CDOrgAlb.getYear() != 0) {
            TempStr += " " + CDOrgAlb.getYear();
        }
        if (CDOrgAlb.getGenre() != "undefiniert") { //TODO sinnvoll??
            TempStr += " " + CDOrgAlb.getGenre();
        }
        //TempStr += " " + CDOrgAlb.getMedium();
        System.out.println("Search-Query: "+TempStr);
        return TempStr;

    }
}
