/*
 * Dieser Thread dient zur Überprüfung der Eingaben des Benutzer in die für die
 * Suche relevanten Felder.
 * Dabei wird eine Suche nur dann ausgelöst, wenn das Entsprechende Feld mehr als
 * 3 Zeichen enthält und nach der letzten Eingabe für mindestens 1sek keine 
 * weitere Eingabe erfolgt ist
 */
package cdorganizer;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Philipp
 */
public class SearchCheckerThread extends Thread {

    private Boolean running = false;
    private String oldInput = "";
    private String newInput = "";

    public SearchCheckerThread() {
        super();
    }

    @Override
    public void run() {
        /*
         * der Thread arbeitet immer, führt den Check aber nur aus wenn die
         * "running"-Variable true gesetzt ist.
         */
        while (running) {
            if (!oldInput.equals(newInput)) {
                System.out.println("test");
                oldInput = newInput;
            }
        }
    }

    public void checkInput(String in) {
        newInput = in;
        running = true;
    }
}
