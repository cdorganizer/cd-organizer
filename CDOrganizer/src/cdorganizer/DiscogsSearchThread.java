/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cdorganizer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;

/**
 *
 * @author Philipp
 */
public class DiscogsSearchThread extends Thread {

    private DiscogsInterface discogsInterface;
    private CDOrgAlbum requestAlbum;
    private ArrayList<CDOrgAlbum> results;
    private Boolean running = true;
    private cdorgController cdorgctrl;
    private Integer counter=0;

    public DiscogsSearchThread(CDOrgAlbum request, Boolean running, cdorgController cdorgctrl) {
        super();
        discogsInterface = new DiscogsInterface();
        requestAlbum = new CDOrgAlbum();
        requestAlbum = request;
        results = new ArrayList<>();
        this.running = running;
        this.cdorgctrl = cdorgctrl;
    }

    @Override
    public void run() {
        if (running) {
            //System.out.println("running");
            try {
                results = discogsInterface.discogsRequest(requestAlbum, 25);
                cdorgctrl.setSearchedAlbums(results);
            } catch (JSONException | IOException ex) {
                Logger.getLogger(DiscogsSearchThread.class.getName()).log(Level.SEVERE, null, ex);
            }
            running = false;
            //System.out.println("stopped");
        }
        //counter++;
        //System.out.println(counter);
    }

    public CDOrgAlbum getResult(int index) {
        return results.get(index);
    }

    public ArrayList<CDOrgAlbum> getAllResults() {
        return results;
    }

    public void setRequestAlbum(CDOrgAlbum album) {
        requestAlbum = album;
    }

    public void pauseThread() {
        running = false;
    }
    
    public void resumeThread(){
        running = true;
    }
}
