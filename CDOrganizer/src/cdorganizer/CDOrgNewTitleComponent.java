/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cdorganizer;

import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Region;

/**
 *
 * @author Philipp
 */
public class CDOrgNewTitleComponent extends Region {

    private final TextField titleField;
    private final Button deleteButton;
    //private final Button browseFileButton;
    private Integer top;
    private Integer left;
    private final AnchorPane parent;
    //private final JFileChooser fc;
    private final CDOrgTitle cdTitle;
    private Boolean deleted;
    private Integer index;

    public CDOrgNewTitleComponent(CDOrgTitle title, AnchorPane parentPane, Integer top, Integer left, Integer index) {
        titleField = new TextField();
        deleteButton = new Button();
        //browseFileButton = new Button();
        //fc = new JFileChooser();

        this.top = top;
        this.left = left;
        cdTitle = title;
        deleted = false;
        parent = parentPane;
        this.index = index;

        titleField.setLayoutX(left);
        titleField.setLayoutY(top);
        titleField.setPrefWidth(125);
        titleField.setEditable(false);
        titleField.setText(cdTitle.getTitleName());

        deleteButton.setLayoutY(top);
        deleteButton.setLayoutX(left + 130);
        deleteButton.setPrefWidth(29);
        deleteButton.setPrefHeight(22);
        deleteButton.setText("-");

        /*browseFileButton.setLayoutY(top);
         browseFileButton.setLayoutX(left + 165);
         browseFileButton.setPrefHeight(22);
         browseFileButton.setPrefWidth(29);
         browseFileButton.setText("...");*/

        parent.getChildren().add(titleField);
        parent.getChildren().add(deleteButton);
        //parent.getChildren().add(browseFileButton);

        /*deleteButton.setOnAction(new EventHandler<ActionEvent>() {
         @Override
         public void handle(ActionEvent t) {
         parent.getChildren().remove(titleField);
         parent.getChildren().remove(deleteButton);
         //parent.getChildren().remove(browseFileButton);
         deleted = true;
         }
         });*/



        /*browseFileButton.setOnAction(new EventHandler<ActionEvent>() {
         @Override
         public void handle(ActionEvent t) {
         fc.addChoosableFileFilter(new FileNameExtensionFilter("Audiodateien", "mp3", "wma"));
         fc.setAcceptAllFileFilterUsed(true);
         int returnVal = fc.showOpenDialog(null);

         if (returnVal == JFileChooser.APPROVE_OPTION) {
         File file = fc.getSelectedFile();
         cdTitle.setTitlepath(file.getAbsolutePath());
         }
         }
         });*/
    }

    public void delete() {
        parent.getChildren().remove(this.titleField);
        parent.getChildren().remove(this.deleteButton);
        deleted = true;
    }

    public TextField getTextField() {
        return titleField;
    }

    public Button getDeleteButton() {
        return deleteButton;
    }

    public Boolean isDeleted() {
        return deleted;
    }
    /* public Button getBrowseFileButton() {
     return browseFileButton;
     }*/

    public Integer getIndex() {
        return this.index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public String getName() {
        return cdTitle.getTitleName();
    }

    public String getFilePath() {
        return cdTitle.getTitlepath();
    }

    public Boolean getIsDeleted() {
        return deleted;
    }

    public void setLayoutX(Double x) {
        this.left = x.intValue();
        titleField.setLayoutX(left);
        deleteButton.setLayoutX(left + 130);
        // browseFileButton.setLayoutX(left + 165);
    }

    public void setLayoutY(Double y) {
        this.top = y.intValue();
        titleField.setLayoutY(top);
        deleteButton.setLayoutY(top);
        // browseFileButton.setLayoutY(top);
    }
}
