/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cdorganizer;

import java.util.ArrayList;

/**
 *
 * @author ab
 */
public class SQLiteInterfaceTest {
     public static void main(String[] args) throws Exception{
         CDOrgSQLiteInterface DBI = new CDOrgSQLiteInterface();
         DBI.openDB("C:\\Users\\ab\\Desktop\\test.db");
         // add first album
         CDOrgAlbum CD = new CDOrgAlbum();
         CD.setGenre("Thrash-Metal");
         CD.setCD(true);
         CD.setDigital(true);
         CD.setInterpret("Metallica");
         CD.setName("My Best of :)");
         ArrayList<CDOrgTitle> Titles = new ArrayList<CDOrgTitle>();
         CDOrgTitle Title = new CDOrgTitle(1,5.25,"Seek and Destroy","");
         Titles.add(Title);
         Title = new CDOrgTitle(2,10.5,"The Unnamed Feeling","");
         Titles.add(Title);
         Title = new CDOrgTitle(3,8.2,"Orion","");
         Titles.add(Title);
         CD.setTitles(Titles);
         DBI.addCDOrgAlbum(CD);
         
         //add second album
         CD.setName("My second Best of :D");
         DBI.addCDOrgAlbum(CD);
         
         //search 
         CDOrgAlbum CD2 = new CDOrgAlbum();
         CD2.setInterpret("Metallica");
         DBI.searchCDOrgAlbum(CD2);
    }
}
