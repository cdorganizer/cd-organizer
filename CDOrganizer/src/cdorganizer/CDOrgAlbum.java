package cdorganizer;

import java.util.ArrayList;

/**
 *
 * @author Philipp, Dennis
 */
public class CDOrgAlbum {

    private String name;
    private String eanNumber;
    private String genre;
    private String interpret;
    private ArrayList<CDOrgTitle> titles;
    private String cover;
    private Integer rating;
    private Integer year;
    //private Integer titleCount = 0;
    private Boolean isCompilation;
    private Boolean isCD;
    private Boolean isDigital;
    private Boolean isVinyl;
    private Boolean isTape;

    public CDOrgAlbum() {
        super();
        titles = new ArrayList<>();

        this.name = "";
        this.genre = ""; 
        this.genre = ""; //TODO leerstring nicht sinnvoller??
        this.eanNumber = "";
        this.interpret = "";
        this.cover = System.getProperty("user.dir")+"\\pics\\cover-template.png";
        //this.cover = "D:\\Repository\\cd-organizer\\CDOrganizer\\cover-template.png";
        //this.cover = "C:\\Users\\Philipp\\Documents\\cd-organizer\\CDOrganizer\\no-icon.png";
        this.isCD = false;
        this.isCompilation = false;
        this.isDigital = false; //TODO false ???
        this.isTape = false;
        this.isVinyl = false;
        this.rating = 0;
        //this.titleCount = 0;
        this.year = 0;
    }

    private boolean checkYear(String y) {
        if (y.length() <= 4) {
            return true;
        } else {
            return false;
        }

    }

    private void fetchImage(String uri) {
        //TODO
        //Check ob das Bildformat unterstützt wird
    }

    public CDOrgTitle getTitle(Integer index) {
        return titles.get(index);
    }

    public String getMedium() {
        String medium = "";
        if (isCD) {
            medium += "CD, ";
        }
        if (isDigital) {
            medium += "Digital, ";
        }
        if (isVinyl) {
            medium += "Vinyl, ";
        }
        if (isTape) {
            medium += "Kassette, ";
        }
        medium = medium.substring(0, medium.length());
        return medium;
    }

    public String getName() {
        return name;
    }

    public String getEAN() {
        return eanNumber;
    }

    public String getGenre() {
        return genre;
    }

    public String getInterpret() {
        return interpret;
    }

    public Integer getTitleCount() {
        return titles.size();
    }

    public String getCover() {
        return cover;
    }

    public Integer getRating() {
        return rating;
    }

    public Integer getYear() {
        return year;
    }

    public Boolean getIsCompilation() {
        return isCompilation;
    }

    public Boolean getIsCD() {
        return isCD;
    }

    public Boolean getIsDigital() {
        return isDigital;
    }

    public Boolean getIsTape() {
        return isTape;
    }

    public Boolean getIsVinyl() {
        return isVinyl;
    }

    public void setName(String n) {
        name = n;
    }

    public void setEAN(String ean) {
        eanNumber = ean;
    }

    public void setGenre(String gen) {
        genre = gen;
    }

    public void setInterpret(String interpret) {
        this.interpret = interpret;
    }

    public void addTitle(CDOrgTitle title) {
    }

    public void setTitles(ArrayList<CDOrgTitle> titles) {
        this.titles = titles;
    }

    public void setCover(String path) {
        //cover = path;
        this.cover = System.getProperty("user.dir")+"\\pics\\cover-template.png";
    }

    public void setRating(Integer rate) {  //Per Exception
        if (rate >= 0 & rate <= 10) {
            rating = rate;
        }
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public void setYear(String year) {
        this.year = Integer.parseInt(year);
    }

    public void setCompilation(Boolean compilation) {
        isCompilation = compilation;
    }

    public void setCD(Boolean cd) {
        isCD = cd;
    }

    public void setDigital(Boolean digital) {
        isDigital = digital;
    }

    public void setVinyl(Boolean vinyl) {
        isVinyl = vinyl;
    }

    public void setTape(Boolean tape) {
        isTape = tape;
    }
}
