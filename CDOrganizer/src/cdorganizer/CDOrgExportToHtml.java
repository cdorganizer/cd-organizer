/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cdorganizer;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Scanner;
import javafx.scene.shape.Path;

/**
 *
 * @author Philipp
 */
public class CDOrgExportToHtml {

    private cdorgController controller;
    private String description = "";
    private String title = "";

    public CDOrgExportToHtml(cdorgController cdctrl) {
        super();
        controller = cdctrl;
    }

    public void exportCurrentSearchResults(String filePath) throws FileNotFoundException, IOException {
        ArrayList<CDOrgAlbum> tempAlbums;
        tempAlbums = controller.getSearchResults();
        String albums = "";
        for (int i = 0; i < tempAlbums.size(); i++) {
            albums = albums.concat("\n"+convertCDOrgAlbum(tempAlbums.get(i))+"\n");
        }
        String content = new Scanner(new File(System.getProperty("user.dir")+"\\export\\template.html")).useDelimiter("\\A").next();
        content = content.replace("[title]", title);
        //content = content.replaceFirst("[description]", description);
        content = content.replace("[album_objects]", albums);
        
        java.nio.file.Path target = Paths.get(filePath);
        InputStream is = new ByteArrayInputStream(content.getBytes());
        Files.copy(is, target, StandardCopyOption.REPLACE_EXISTING);
        
    }

    private String convertCDOrgAlbum(CDOrgAlbum album) {
        String temp = "<div class=\"row-fluid\">\n"
                + "<div class=\"span2\">\n"
                + "     <img src=\"" + album.getCover() + "\" alt=\"Cover\">\n"
                + "</div>\n"
                + "<p></p>"
                + "<div class=\"span10\">\n"
                + "	<font size=\"6\"><b>" + album.getInterpret() + "</b>&nbsp - &nbsp <b>" + album.getName() + "</b></font>	\n"
                + "	<font size=\"4\"><p><b>" + album.getYear() + "&nbsp&nbsp&nbsp&nbsp - &nbsp&nbsp&nbsp&nbsp" + album.getGenre() + "&nbsp&nbsp&nbsp&nbsp - &nbsp&nbsp&nbsp&nbspVorhanden als:" + album.getMedium() + "</b></p></font>\n"
                + "	<font size=\"3\"><p>"+description+"</p></font> <!--optional-->\n"
                + "</div>		\n"
                + "<table class=\"table table-hover\">\n"
                + "	<tr>\n"
                + "		<th>#</th>\n"
                + "		<th>Titel</th>\n"
                + "		<th>Dauer</th>\n"
                + "	</tr>	\n";
        String titles = "";
        for (int i = 0; i < album.getTitleCount(); i++) {
            titles = titles.concat("<tr>\n"
                    + "<td><font size=\"2\">" + album.getTitle(i).getTitleNr() + "</font></td>\n"
                    + "<td><font size=\"2\">" + album.getTitle(i).getTitleName() + "</font></td>\n"
                    + "<td><font size=\"2\">" + album.getTitle(i).getTitleLength() + "</font></td>\n</tr>\n");
        }
        return temp + titles + "</table>\n </div>\n <hr></hr><p></p>\n";
    }

    public void setPageTitle(String title) {
        this.title = title;
    }

    public void setDescription(String descr) {
        description = descr;
    }
}
