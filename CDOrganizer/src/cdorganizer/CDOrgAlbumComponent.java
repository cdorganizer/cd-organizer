package cdorganizer;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javax.imageio.ImageIO;

/**
 *
 * @author Philipp
 */
public class CDOrgAlbumComponent {

    private final CDOrgAlbum albumData;
    private final ImageView coverImage;
    private final Image tempImage;
    private final BufferedImage tempBufferImg;
    private final Text albumText;
    private final Text interpretText;
    private final Text yearText;
    private final Text mediumText;
    private final Text ratingBox;
    private final Button expandButton;
    private Boolean isExpanded;
    private final Double height;
    private final Integer top;
    private final Integer left;
    private final Integer index;
    private final AnchorPane parent;
    private TableView titleList = new TableView();
    private final ObservableList<CDOrgTitle> data = FXCollections.observableArrayList();

    /**
     *
     * @param parentPane
     * @param albumData
     * @param top
     * @param left
     * @param index
     * @throws IOException
     */
    public CDOrgAlbumComponent(AnchorPane parentPane, CDOrgAlbum albumData, Integer top, Integer left, Integer index) throws IOException {
        /*assert layout.length >= 0 && layout.length <= 3;
         if (layout.length == 1) {
         top = layout[0];
         } else if (layout.length == 2) {
         top = layout[0];
         left = layout[1];
         }
         this.index = layout[2];*/
        this.index = index;
        this.top = top;
        this.left = left;
        this.albumData = albumData;

        parent = parentPane;
        /*
         * Der übergebene Pfad wird hier in 2 schritten so umgewandelt, das dass
         * ImageView richtig geladen wird, dazu wird zunächst ein File aud dem pfad erzeugt
         * und dieses dann zuerst zu einem BufferedImage gemacht und dieses wiederum zu einem
         * Image, welches dann vom ImageView geladen wird.
         */
        tempBufferImg = ImageIO.read(new File(albumData.getCover()));
        tempImage = SwingFXUtils.toFXImage(tempBufferImg, null);

        coverImage = new ImageView(tempImage);
        albumText = new Text();
        interpretText = new Text();
        yearText = new Text();
        mediumText = new Text();
        ratingBox = new Text();
        expandButton = new Button();
        isExpanded = false;

        expandButton.setLayoutY(top);
        expandButton.setLayoutX(left);
        expandButton.setPrefHeight(21);
        expandButton.setPrefWidth(29);
        expandButton.setText("+");

        coverImage.setLayoutY(top);
        coverImage.setLayoutX(left + expandButton.getLayoutBounds().getWidth() + 35);
        coverImage.setFitWidth(128);
        coverImage.setFitHeight(128);
        coverImage.setPickOnBounds(true);
        coverImage.setPreserveRatio(true);
        //coverImage.setImage(new Image("Unbennant-1.jpg"));

        albumText.setLayoutY(top + 12);
        albumText.setLayoutX(left + 168);
        albumText.setText(albumData.getName());

        interpretText.setLayoutY(top + 12);
        interpretText.setLayoutX(left + albumText.getLayoutX() + albumText.getLayoutBounds().getWidth() + 25);
        interpretText.setText(albumData.getInterpret());

        ratingBox.setLayoutY(top + 12);
        ratingBox.setLayoutX(left + interpretText.getLayoutX() + interpretText.getLayoutBounds().getWidth() + 10);
        ratingBox.setText("Bewertung: " + albumData.getRating().toString());

        yearText.setLayoutY(top + 45);
        yearText.setLayoutX(albumText.layoutXProperty().intValue());
        yearText.setText(albumData.getYear().toString());

        mediumText.setLayoutY(top + 45);
        mediumText.setLayoutX(interpretText.layoutXProperty().intValue());
        mediumText.setText(albumData.getMedium());

        titleList = createTableView();
        titleList.setLayoutX(left + albumText.getLayoutX());
        titleList.setLayoutY(interpretText.getLayoutY() + 50);
        height = coverImage.getFitHeight();

        parent.getChildren().add(expandButton);
        parent.getChildren().add(coverImage);
        parent.getChildren().add(albumText);
        parent.getChildren().add(yearText);
        parent.getChildren().add(interpretText);
        parent.getChildren().add(mediumText);
        parent.getChildren().add(ratingBox);
        parent.getChildren().add(titleList);
        this.Collapse();

        for (Integer i = 0; i < albumData.getTitleCount(); i++) {
            data.add(albumData.getTitle(i));
        }
        /*expandButton.setOnAction(new EventHandler<ActionEvent>() {
         @Override
         public void handle(ActionEvent e) {
         handleExpandButtonAction(e);
         //data.add(new CDOrgTitle(1, "tetststst", 2.4));
         }
         });*/

    }

    private TableView createTableView() {
        TableColumn<CDOrgTitle, Integer> nrCol = new TableColumn<>("Titel Nr.");
        nrCol.setCellValueFactory(new PropertyValueFactory<CDOrgTitle, Integer>("titleNr"));

        TableColumn<CDOrgTitle, String> nameCol = new TableColumn<>("Titel Name");
        nameCol.setCellValueFactory(new PropertyValueFactory<CDOrgTitle, String>("titleName"));

        TableColumn<CDOrgTitle, Double> lengthCol = new TableColumn<>("Titel Länge");
        lengthCol.setCellValueFactory(new PropertyValueFactory<CDOrgTitle, Double>("titleLength"));

        TableView<CDOrgTitle> titles = new TableView<>();
        titles.setItems(data);

        titles.getColumns().addAll(nrCol, nameCol, lengthCol);
        titles.setLayoutY(mediumText.layoutYProperty().intValue() + 20);
        titles.setLayoutX(left + expandButton.getLayoutBounds().getWidth() + 50);
        titles.setPrefWidth(parent.getWidth() - left - coverImage.getFitWidth() - 20);

        nrCol.setPrefWidth(titles.getPrefWidth() / 3);
        nameCol.setPrefWidth(titles.getPrefWidth() / 3);
        lengthCol.setPrefWidth(titles.getPrefWidth() / 3);
        return titles;
    }

    /**
     *
     */
    public final void Expand() {
        isExpanded = true;
        expandButton.setText("-");
        titleList.setVisible(true);
    }

    /**
     *
     */
    public final void Collapse() {
        isExpanded = false;
        expandButton.setText("+");
        titleList.setVisible(false);
    }

    /**
     *
     * @return
     */
    public Boolean getState() {
        return isExpanded;
    }

    /**
     *
     * @return
     */
    public Integer getTop() {
        return top;
    }

    /**
     *
     * @return
     */
    public AnchorPane getParent() {
        return parent;
    }

    /**
     *
     * @param x
     */
    public void setLayoutX(Integer x) {
        //Move ALL Objects in x-Direction
    }

    /**
     *
     * @param y
     */
    public void setLayoutY(Double y) {
        expandButton.setLayoutY(y);
        coverImage.setLayoutY(y);
        albumText.setLayoutY(y + 12);
        interpretText.setLayoutY(y + 12);
        ratingBox.setLayoutY(y + 12);
        yearText.setLayoutY(y + 45);
        mediumText.setLayoutY(y + 45);
        titleList.setLayoutY(mediumText.layoutYProperty().intValue() + 20);
    }

    /*public void handleExpandButtonAction(ActionEvent e) {
     if (isExpanded) {
     isExpanded = false;
     expandButton.setText("+");
     titleList.setVisible(false);
     } else {
     isExpanded = true;
     expandButton.setText("-");
     titleList.setVisible(true);
     }
     }*/
    /**
     *
     * @return
     */
    public Double getHeight() {
        return height;
    }

    /**
     *
     * @return
     */
    public Double getTableHeight() {
        return titleList.getHeight();
    }

    /**
     *
     * @return
     */
    public Button getExpandButton() {
        return expandButton;
    }

    /**
     *
     * @return
     */
    public ImageView getCoverimage() {
        return coverImage;
    }

    /**
     *
     * @return
     */
    public Integer getIndex() {
        return index;
    }

    /**
     *
     * @return
     */
    public CDOrgAlbum getAlbum() {
        return albumData;
    }

    public void deleteComponent() {
        parent.getChildren().remove(coverImage);
        parent.getChildren().remove(albumText);
        parent.getChildren().remove(interpretText);
        parent.getChildren().remove(yearText);
        parent.getChildren().remove(mediumText);
        parent.getChildren().remove(ratingBox);
        parent.getChildren().remove(expandButton);
        parent.getChildren().remove(titleList);

    }

    public void shiftUp(int value) {
        expandButton.setLayoutY(expandButton.getLayoutY() - value);
        coverImage.setLayoutY(coverImage.getLayoutY() - value);
        albumText.setLayoutY(albumText.getLayoutY() - value);
        interpretText.setLayoutY(interpretText.getLayoutY() - value);
        ratingBox.setLayoutY(ratingBox.getLayoutY() - value);
        yearText.setLayoutY(yearText.getLayoutY() - value);
        mediumText.setLayoutY(mediumText.getLayoutY() - value);
        titleList.setLayoutY(titleList.getLayoutY() - value);
    }

    public void shiftDown(int value) {
        expandButton.setLayoutY(expandButton.getLayoutY()+value);
        coverImage.setLayoutY(coverImage.getLayoutY()+value);
        albumText.setLayoutY(albumText.getLayoutY()+value);
        interpretText.setLayoutY(interpretText.getLayoutY()+value);
        ratingBox.setLayoutY(ratingBox.getLayoutY()+value);
        yearText.setLayoutY(yearText.getLayoutY()+value);
        mediumText.setLayoutY(mediumText.getLayoutY()+value);
        titleList.setLayoutY(titleList.getLayoutY()+value);
    }
}
