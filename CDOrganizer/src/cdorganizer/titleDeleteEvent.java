/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cdorganizer;

import java.util.ArrayList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.layout.AnchorPane;

/**
 *
 * @author Philipp
 */
public class titleDeleteEvent extends cdorgController {

    private Integer titleIndex;
    private final EventHandler click;

    public titleDeleteEvent(Integer index, final ArrayList<CDOrgNewTitleComponent> newTitles, final AnchorPane parent, cdorgController cdctrl) {
        super();
        titleIndex = index;
        click = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                /*System.out.println(titleIndex.toString());
                 System.out.println(newTitles.get(titleIndex).getName());*/
                if (newTitles.size() == 1){
                    titleIndex--;
                }
                parent.getChildren().remove(newTitles.get(titleIndex).getTextField());
                parent.getChildren().remove(newTitles.get(titleIndex).getDeleteButton());
                //System.out.println("before:    " + newTitles.size() + "  " + titleIndex);

                newTitles.subList(titleIndex, titleIndex + 1).clear();

                //newTitles.trimToSize();
                //System.out.println("after:    " + newTitles.size() + "  " + titleIndex);

                for (Integer i = titleIndex; i <= newTitles.size() - 1; i++) {
                    newTitles.get(i).getDeleteButton().setLayoutY(newTitles.get(i).getDeleteButton().getLayoutY() - 25);
                    newTitles.get(i).getTextField().setLayoutY(newTitles.get(i).getTextField().getLayoutY() - 25);
                    newTitles.get(i).setIndex(i - 1);
                }
            }
        };
    }

    public Integer getIndex() {
        return titleIndex;
    }

    public EventHandler getEvent() {
        return click;
    }
}
