/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cdorganizer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

/**
 *
 * @author Philipp
 */
public class CDOrgDisplaySearchresultsTask extends TimerTask {

    private final cdorgController controller;
    private int lastResultCount;

    public CDOrgDisplaySearchresultsTask(cdorgController cdctrl) {
        super();
        controller = cdctrl;
        lastResultCount = 0;
    }

    @Override
    public void run() {
        if (controller.getSearchedResultsCount() > lastResultCount) {//neue Suchergebnisse vorhanden
            if (controller.getSearchedResultsCount() >= 10) {
                controller.cleanupSearchResults();
            }
            lastResultCount = controller.getSearchedResultsCount();

            /*
             * Das ausführen der Methode "ShowSearchresults()" muss im application-Thread
             * geschehen, deshab wird der Aufruf hier dem application-thread hinzugefügt
             */
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    try {
                        controller.showSearchResults(CDOrgDisplaySearchresultsTask.this.lastResultCount);
                    } catch (IOException ex) {
                        Logger.getLogger(CDOrgDisplaySearchresultsTask.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            });


        } else {
            System.out.println("Überprüfung fand keine neuen Suchergebnisse");
        }

    }

    private void showSearchResults() throws IOException {
        AnchorPane parent;
        ArrayList<CDOrgAlbum> results;
        results = controller.getSearchedAlbums().get(lastResultCount - 1);
        if (controller.getActiveTab() == 0) {
            parent = controller.getLocalDatabaseScrollAnchor();
        } else {
            parent = controller.getDiscogsDatabaseScrollAnchor();
        }
        parent.setPrefHeight(676);
        for (int i = 0; i < controller.getAlbumComponents().size(); i++) {
            controller.getAlbumComponents().get(i).deleteComponent();
        }
        controller.getAlbumComponents().clear();
        for (int i = 0; i < results.size(); i++) {
            controller.getAlbumComponents().add(new CDOrgAlbumComponent(parent, results.get(i), 10 + (controller.getAlbumComponents().size() * 135), 10, controller.getAlbumComponents().size()));
            controller.getAlbumEvents().add(new albumExpandEvent(controller.getAlbumComponents().size() - 1, controller.getAlbumComponents(), parent, controller));
            controller.getAlbumComponents().get(controller.getAlbumComponents().size() - 1).getExpandButton().setOnAction(controller.getAlbumEvents().get(controller.getAlbumEvents().size() - 1).getExpandEvent());
            controller.getAlbumComponents().get(controller.getAlbumComponents().size() - 1).getCoverimage().setOnMousePressed(controller.getAlbumEvents().get(controller.getAlbumEvents().size() - 1).getClickEvent());
        }
    }
}
